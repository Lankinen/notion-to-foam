import os

from remove_id import remove_id, parse_uuid
from rename_links_in_file import rename_links_in_file
from fix_links_in_file import fix_links_in_file
from add_file_id_to_links import add_file_id_to_links
from constants import MAIN_FILE_NAME, moved_files
from utils import ends_with_any


def crawler(dir_path: str, file_callback, dir_callback=None):
    assert dir_path[-1] == "/"
    for item in sorted(os.listdir(dir_path)):
        if os.path.isfile(dir_path + item):
            file_callback(dir_path, item)
        else:
            new_dir_name = dir_callback(
                dir_path, item) if dir_callback else item
            crawler(dir_path + new_dir_name + "/", file_callback, dir_callback)


def rename_files_and_folders(_dir_path: str):
    def file_callback(path: str, item: str):
        if ends_with_any(item, [".md", ".csv"]):
            new_name = remove_id(item, path)
            os.rename(path + item, path + new_name)

    def dir_callback(path: str, item: str):
        new_name = remove_id(item, path)
        os.rename(path + item, path + new_name)
        return new_name

    crawler(_dir_path, file_callback, dir_callback)


def rename_links(_dir_path: str):
    def file_callback(path: str, item: str):
        full_path = path + item
        if ends_with_any(item, [".md", ".csv"]):
            rename_links_in_file(full_path)

    crawler(_dir_path, file_callback)


def move_index(_path: str):
    def file_callback(path: str, item: str):
        if ends_with_any(item, [".md", ".csv"]):
            full_path = path + item
            (directory_path, _) = os.path.splitext(full_path)
            if os.path.isdir(directory_path):
                # TODO: check that the same name file doesn't exist
                main_file_extension = ".csv" if full_path.endswith(
                    ".csv") else ".md"
                new_file_path = directory_path + "/" + MAIN_FILE_NAME + main_file_extension
                os.rename(full_path, new_file_path)
                moved_files.add(directory_path)

    crawler(_path, file_callback)


def fix_links_from_to_moved(_path: str):
    def file_callback(path: str, item: str):
        full_path = path + item
        if item.endswith(".md"):
            fix_links_in_file(path, full_path, moved_files)

    crawler(_path, file_callback)


def fix_for_nextcloud(_path: str):
    def file_callback(path: str, item: str):
        full_path = path + item
        if item.endswith(".md"):
            add_file_id_to_links(full_path)

    crawler(_path, file_callback)


def remove_filter_csvs(_path: str):
    remove_csvs = set()

    def file_callback_find_files(_, item: str):
        # Detect files we want to remove
        if ends_with_any(item, [".csv"]):
            [name, uuid, _] = parse_uuid(item)
            if uuid.endswith("_all"):
                remove_csvs.add(
                    name + " " + uuid.replace("_all", "") + ".csv")

    def file_callback_remove_files(path: str, item: str):
        # Remove wanted files
        if ends_with_any(item, [".csv"]):
            if item in remove_csvs:
                os.remove(path + item)

    def file_callback_rename_all_files(path: str, item: str):
        if ends_with_any(item, [".csv"]):
            [name, uuid, _] = parse_uuid(item)
            full_path = path + item
            new_item = name + " " + uuid.replace("_all", "") + ".csv"
            new_file_path = path + new_item
            if uuid.endswith("_all"):
                os.rename(full_path, new_file_path)

    crawler(_path, file_callback_find_files)
    crawler(_path, file_callback_remove_files)
    crawler(_path, file_callback_rename_all_files)


if __name__ == "__main__":
    root_path = input("Enter a path to your notes: ")
    if not root_path.endswith("/"):
        root_path += "/"

    print("Starting conversion...")
    remove_filter_csvs(root_path)
    rename_files_and_folders(root_path)
    rename_links(root_path)
    move_index(root_path)
    fix_links_from_to_moved(root_path)
    # fix_for_nextcloud(root_path)
    print("Conversion ready!")
