import re

# NextCloud requires to have `?fileId=0` in the end of relative links for them to work
def add_file_id_to_links(full_path: str):
    with open(full_path, 'r', encoding="utf-8") as file:
        data = file.read()

    links = re.findall(r'\[(.+)\]\((.+)(\.(?:md|csv))\)', data)
    for name, og_link, extension in links:
        old_full_link = f"[{name}]({og_link}{extension})"
        new_full_link = f"[{name}]({og_link}{extension}?fileId=0)"
        data = data.replace(old_full_link, new_full_link)

    with open(full_path, 'w', encoding="utf-8") as file:
        file.write(data)
