import urllib.parse
import re

from remove_id import remove_id
from utils import ends_with_any

def rename_links_in_file(full_path: str):
    with open(full_path, 'r', encoding="utf-8") as file:
        data = file.read()

    links = re.findall(r'!?\[(.+)\]\(((?!https:\/\/|http:\/\/).+\.(?:md|csv|png|jpg|jpeg))\)', data)
    for (name, og_link) in links:
        clean_link = urllib.parse.unquote(og_link)
        parts = []
        for part_link in clean_link.split("/"):
            is_markdown_or_csv = "." in part_link and ends_with_any(part_link, [".md", ".csv"])
            is_dir = "." not in part_link
            if is_markdown_or_csv or is_dir:
                part_link = remove_id(part_link, "/".join(parts))
            parts.append(part_link)
        clean_link = "/".join(parts)

        clean_link_quoted = urllib.parse.quote(clean_link)
        if ends_with_any(clean_link, [".md", ".csv"]):
            data = data.replace(f"[{name}]({og_link})", f"[{name}]({clean_link_quoted})")
        else:
            data = data.replace(f"![{name}]({og_link})", f"![{clean_link}]({clean_link_quoted})")

    with open(full_path, 'w', encoding="utf-8") as file:
        file.write(data)
