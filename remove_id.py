from uuid import UUID
import os

uuid_to_custom_id = dict()


def _is_valid_uuid(test_uuid, version=4):
    try:
        UUID(hex=test_uuid, version=version)
    except ValueError:
        return False
    return True


def _find_unique_name(path: str, cleaned_name: str, uuid: str, extension: str = None):
    custom_id = 0
    if uuid in uuid_to_custom_id:
        cleaned_name += " " + uuid_to_custom_id[uuid]
    else:
        while True:
            full_path = path + cleaned_name
            if custom_id > 0:
                full_path += " " + str(custom_id)
            if extension:
                full_path += "." + extension
            already_exists = os.path.isfile(
                full_path) or os.path.isdir(full_path)
            if already_exists:
                custom_id += 1
            else:
                if custom_id > 0:
                    uuid_to_custom_id[uuid] = str(custom_id)
                    cleaned_name += " " + str(custom_id)
                break
    if extension:
        cleaned_name += "." + extension
    return cleaned_name


def parse_uuid(name: str):
    parts = name.split(" ")
    assert len(parts) > 1, f"Not enough parts -> {name}"
    last = parts[-1]
    cleaned_name = " ".join(parts[:-1])
    uuid = last.split(".")[0] if "." in last else last
    return [cleaned_name, uuid, last]


def remove_id(name: str, path: str):
    [cleaned_name, uuid, last] = parse_uuid(name)
    is_last_valid = _is_valid_uuid(uuid)
    assert is_last_valid, f"Last part not an valid UUID -> {name}"

    if "." in last:
        extension = last.split(".")[-1]
        cleaned_name = _find_unique_name(path, cleaned_name, uuid, extension)
        return cleaned_name
    cleaned_name = _find_unique_name(path, cleaned_name, uuid)
    return cleaned_name
