# pylint: disable=line-too-long,unnecessary-lambda
import unittest
from unittest.mock import patch, mock_open

from rename_links_in_file import rename_links_in_file
from fix_links_in_file import fix_links_in_file
from constants import MAIN_FILE_NAME

def helper_rename_links_in_file(raw: str):
    path = "./data/Main/"
    item = "Inspiration.md"
    full_path = path + item
    with patch('builtins.open', mock_open(read_data=raw)) as file:
        rename_links_in_file(full_path)
    file.assert_called_with(full_path, 'w', encoding="utf-8")
    handle = file()
    return lambda x: handle.write.assert_called_once_with(x)

def helper_fix_links(raw: str, moved_files: dict):
    with patch('os.path.isdir', return_value=True):
        path = "./data/Main/Inspiration/"
        item = MAIN_FILE_NAME + ".md"
        full_path = path + item
        with patch('builtins.open', mock_open(read_data=raw)) as file:
            fix_links_in_file(path, full_path, moved_files)
        file.assert_called_with(full_path, 'w', encoding="utf-8")
        handle = file()
        return lambda x: handle.write.assert_called_once_with(x)

class Test(unittest.TestCase):
    def test_rename_links_in_file_img(self):
        raw = "![Inspiration%20Test%2069a60ee41a5348c6a865cf83bf118b46/Untitled.png](Inspiration%20Test%2069a60ee41a5348c6a865cf83bf118b46/Untitled.png)"
        assert_cb = helper_rename_links_in_file(raw)
        assert_cb("![Inspiration Test/Untitled.png](Inspiration%20Test/Untitled.png)")

    def test_rename_links_in_file_md(self):
        raw = "[Test file](Inspiration%2069a60ee41a5348c6a865cf83bf118b46/Untitled%20b4453b8b45514909934e1ba6527b76cf/Test%20file%209aba36a74017421a82f8102ae56ff214.md)"
        assert_cb = helper_rename_links_in_file(raw)
        assert_cb("[[Inspiration/Untitled/Test file.md]]")

    def test_from_fix_links_in_file_img(self):
        raw = "![Inspiration/2B97F5D1-A60E-4560-B657-30F9D636081B.png](Inspiration/2B97F5D1-A60E-4560-B657-30F9D636081B.png)"
        moved_files = {'./data/Main/Inspiration': 'Inspiration'}
        pred = "![2B97F5D1-A60E-4560-B657-30F9D636081B.png](./2B97F5D1-A60E-4560-B657-30F9D636081B.png)"
        assert_cb = helper_fix_links(raw, moved_files)
        assert_cb(pred)

    def test_from_fix_links_in_file_md(self):
        raw = "[[Inspiration/test.md]]"
        moved_files = {'./data/Main/Inspiration': 'Inspiration'}
        pred = "[[test.md]]"
        assert_cb = helper_fix_links(raw, moved_files)
        assert_cb(pred)

    def test_to_fix_links_in_file(self):
        raw = "[[test.md]]"
        moved_files = {'./data/Main/Inspiration/test': 'test'}
        pred = f"[[test/{MAIN_FILE_NAME}.md]]"
        assert_cb = helper_fix_links(raw, moved_files)
        assert_cb(pred)

if __name__ == '__main__':
    unittest.main()
