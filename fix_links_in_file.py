import re
import urllib.parse
import os

from constants import MAIN_FILE_NAME

def _link_from_moved_file(path: str, data: str):
    def helper(og_link: str):
        clean_link = urllib.parse.unquote(og_link)
        is_linked_same_level = clean_link.find("/") == -1
        if re.match(r'\.\.', clean_link) or is_linked_same_level:
            clean_link = "../" + clean_link
        else:
            last = path.split("/")[-2]
            first = clean_link.split("/")[0]
            if first == last:
                first_part_removed = "/".join(clean_link.split("/")[1:])
                clean_link = first_part_removed
        clean_link_quoted = urllib.parse.quote(clean_link)
        return clean_link_quoted

    # Images
    image_links = re.findall(r'!\[(.+)\]\((.+\.(?:png|jpg|jpeg))\)', data)
    for (name, og_link) in image_links:
        old_full_link = f"![{name}]({og_link})"
        new_link = helper(og_link)

        new_full_link = f"![{name}]({new_link})"
        data = data.replace(old_full_link, new_full_link)

    # Markdowns
    # md_links = re.findall(r'\[\[(.+)\.md\]\]', data)
    md_links = re.findall(r'\[(.+)\]\((.+)(\.(?:md|csv))\)', data)
    for name, og_link, extension in md_links:
        old_full_link = f"[{name}]({og_link}{extension})"
        new_link = helper(og_link)

        new_full_link = f"[{name}]({new_link}{extension})"
        data = data.replace(old_full_link, new_full_link)

    return data

def _link_to_moved_file(path: str, data: str, moved_files: set):
    # links = re.findall(r'\[\[(.+)\.md\]\]', data)
    links = re.findall(r'\[(.+)\]\((.+)(\.(?:md|csv))\)', data)
    for name, og_link, extension in links:
        old_full_link = f"[{name}]({og_link}{extension})"
        clean_link = urllib.parse.unquote(og_link)

        comb_link = "/".join(path.split("/")[:-1]) + "/" + clean_link
        if (os.path.isdir(comb_link) and comb_link in moved_files) or re.match(r'\.\.', clean_link):
            new_full_link = f"[{name}]({og_link}/{MAIN_FILE_NAME}{extension})"
            data = data.replace(old_full_link, new_full_link)

    return data

def fix_links_in_file(path: str, full_path: str, moved_files: set):
    with open(full_path, 'r', encoding="utf-8") as file:
        data = file.read()

    main_file_extension = ".csv" if full_path.endswith(".csv") else ".md"
    full_path_without_index = full_path.replace(f"/{MAIN_FILE_NAME}{main_file_extension}","")
    if full_path_without_index in moved_files:
        data = _link_from_moved_file(path, data)
    data = _link_to_moved_file(path, data, moved_files)

    with open(full_path, 'w', encoding="utf-8") as file:
        file.write(data)
