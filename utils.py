def ends_with_any(string: str, endings: [str]):
    for ending in endings:
        if string.endswith(ending):
            return True
    return False
